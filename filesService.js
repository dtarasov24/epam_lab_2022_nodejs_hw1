const fs = require('fs');
const path = require('path');

// constants...
const getFilePath = (directory, filename) => path.join(__dirname, `/${directory}/${filename}`);

class HttpError extends Error {
    constructor(message, errorCode) {
        super(message);
        this.code = errorCode;
    }
}

function createFile(req, res, next) {
    // Your code to create the file.
    const { filename, content } = req.body;

    if (!filename) {
        return next(new HttpError("Please specify 'filename' parameter", 400));
    }

    if (!content) {
        return next(new HttpError("Please specify 'content' parameter", 400));
    }

    if (['.log', '.txt', '.json', '.yaml', '.xml', '.js'].includes(path.extname(filename))) {
        try {
            fs.writeFileSync(getFilePath('files', filename), content);
        } catch (err) {
            return next(new HttpError("Server error", 500));
        }
    } else {
        return next(new HttpError("File extension is not supported", 400));
    }

    res.status(200).json({
        "message": "File created successfully"
    });
}

function getFiles(req, res, next) {
    let files;

    try {
        files = fs.readdirSync(path.join(__dirname, '/files'));
    } catch (err) {
        return next(new HttpError("Server error", 500));
    }

    res.status(200).json({
        "message": "Success",
        "files": files
    });
}

const getFile = (req, res, next) => {
    // Your code to get a file content.
    const filename = req.params.filename;
    const filePath = getFilePath('files', filename);

    let data;
    let creationTime;

    if (filename) {
        if (fs.existsSync(filePath)) {
            data = fs.readFileSync(filePath, 'utf8');
            creationTime = fs.statSync(filePath).birthtime;
        } else {
            return next(new HttpError(`No file with ${filename} filename found`, 400));
        }
    } else {
        return next(new HttpError("Server error", 400));
    }

    res.status(200).send({
        "message": "Success",
        "filename": filename,
        "content": data,
        "extension": path.extname(filename).replace('.', ''),
        "uploadedDate": creationTime
    });
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

function editFile(req, res, next) { 
    const filename = req.params.filename;
    const filePath = getFilePath('files', filename);

    const { content } = req.body;

    if (fs.existsSync(filePath)) {
        try {
            fs.writeFileSync(getFilePath('files', filename), content);
        } catch (err) {
            return next(new HttpError("Server error", 500));
        }
    } else {
        return next(new HttpError(`No file with ${filename} filename found`, 400));
    }

    res.status(200).json({
        filename,
        content
    })
}

function deleteFile(req, res, next) {
    const filename = req.params.filename;
    const filePath = getFilePath('files', filename);

    if (fs.existsSync(filePath)) {
        try {
            fs.unlinkSync(filePath);
        } catch (err) {
            return next(new HttpError("Server error", 500));
        }
    } else {
        return next(new HttpError(`No file with ${filename} filename found`, 400));
    }

    res.status(200).json({
        "message": "Success"
    })
}

module.exports = {
    createFile,
    getFiles,
    getFile,
    editFile,
    deleteFile
}
